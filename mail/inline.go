package mail

import (
	"gitee.com/wpkg/message"
)

// A InlineHeader represents a message text header.
type InlineHeader struct {
	message.Header
}
